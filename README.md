# Firebase Chat Tutorial - Android

A simple chat application tutorial that built using Firebase on Android.

![Screenshot](screenshot.png)

## Setup

Import the android code to the android studio

## Step 1:

Layout the Chat Box:

![Screenshot](main_chat_layout.png)

Open the file content_main_chat under app/res/layout and put the copy the code.

## Step 2:

Layout the Chat message Item

![Screenshot](chat_message_item_list.png)

Create a xml layout and copy the code. This will serve as the chat message item container.

## Step 3:

Connect the layout to activities and/or classes

![Screenshot](ChatlistAdapter_population_layout_connection.png)

## Setup

Update [`MainActivity`](/app/src/main/java/com/awesome/chat/MainActivity.java) and replace
`put here the firebase` with a reference to your Firebase.


